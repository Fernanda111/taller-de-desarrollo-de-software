from django.core import paginator
from django.http.response import Http404
from django.shortcuts import get_object_or_404, render, redirect
from .forms import Registro_de_usuarios, DiagramaForm, Actualizar_perfil
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, models, get_user_model
from django.contrib import messages
from .models import Diagrama
from django.core.paginator import Paginator
from django.http import Http404
from datetime import datetime
import json

# Create your views here.
#HOME
def home(request):
    return render(request, "home.html", {'settings': settings})

#REGISTRO
def registro(request):
    data = {
        'form': Registro_de_usuarios()
    }

    if request.method == 'POST':
        formulario = Registro_de_usuarios(data=request.POST)
        if formulario.is_valid():
            formulario.save()
            user = authenticate(username=formulario.cleaned_data['username'], password=formulario.cleaned_data['password1'])
            login(request, user)
            messages.success(request, "Te has registrado exitosamente")
            # Redirigir al home
            return redirect('/')

        else:
            messages.warning(request, "Error en el registro")

        data['form'] = formulario    

    return render(request, 'registration/registro.html', data)

# documents
@login_required
def documentos(request):

    diagramas = Diagrama.objects.filter(autor=request.user)

    if diagramas:
        # put your logic
        pass

    page = request.GET.get('page', 1)

    try:
        paginator = Paginator(diagramas, 5)
        diagramas = paginator.page(page)

    except:
        raise Http404

    data_diag = {
        'entity': diagramas,
        'paginator': paginator
    }

    return render(request, 'documentos.html', data_diag)    

# save diagram
@login_required
def perfil(request):
    if request.method == 'POST':
        form = Actualizar_perfil(request.POST, instance=request.user)
        if form.is_valid():
            user = form.save(commit=False)
            user.set_password(form.cleaned_data['password'])
            user.save()
            user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
            login(request, user)
            return redirect('/')
    else:
        form=Actualizar_perfil(instance=request.user)

    return render(request, "registration/perfil.html", {'form':form })

# delete diagram
@login_required
def eliminar_diagrama(request, id):
    diagrama = get_object_or_404(Diagrama, id=id)

    messages.success(request, "Eliminado correctamente")
    diagrama.delete()

    return redirect(request.META['HTTP_REFERER'])


# diagram
@login_required
def edicion_diagrama(request, id=None):

    _id = id
    data_info = {
        'form': DiagramaForm()
     }

    if request.method == 'GET':

        if _id == None:

            # new diagram

            return render(request, 'edicion_diagrama.html', data_info) 

        if _id != None:
            
            # edit old diagram 

            datos = Diagrama.objects.filter(id=id).values('datos')
            d =  get_object_or_404(Diagrama, id=id)

            data_info = {
                'datos': datos[0].get('datos'),
                'form': DiagramaForm(instance=d)
             }

            return render(request, 'edicion_diagrama.html', data_info)
            
    if request.method == 'POST':

        if _id == None:

            # guardando el new diagram

            autor = Diagrama(autor=request.user)
            form = DiagramaForm(request.POST, instance=autor)

            if form.is_valid():
                obj = form.save()
                x = obj.id

                return redirect(f'/importar_diagrama/{x}')
                
            else:
                print("Fallé")
                pass

        if _id != None:

            # update

            diagram = get_object_or_404(Diagrama, id=id)

            datos = Diagrama.objects.filter(id=id)

            autor = Diagrama(autor=request.user)
            formulario = DiagramaForm(request.POST, instance=autor)

            if formulario.is_valid():
                a = formulario.cleaned_data['datos']
                b = formulario.cleaned_data['titulo']
                c = formulario.cleaned_data['descripcion']

                datos.update(datos=a, titulo=b, descripcion=c, ultima_modificacion=datetime.now())
                
                return redirect(f'/importar_diagrama/{id}')


    return render(request, 'edicion_diagrama.html')


# convertir diagrama
@login_required
def conversion(request, id):

    if request.method == 'GET':

        try:
            datos = Diagrama.objects.filter(id=id).values('datos')
            data = {
                'datos': datos[0].get('datos')
            }

        except Diagrama.DoesNotExist:
            messages.success(request, "Error")
            
    return render(request, 'edicion_diagrama_importado.html', data)


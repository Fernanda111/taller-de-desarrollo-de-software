from django.urls import path
from .views import edicion_diagrama, home, registro, documentos, perfil, eliminar_diagrama, conversion


urlpatterns = [
    path('', home, name="home"),
    path('registro/', registro, name='registro'),
    path('perfil/', perfil, name='perfil'),
    path('eliminar_diagrama/<id>/', eliminar_diagrama, name='eliminar_diagrama'),
    path('importar_diagrama/<id>', edicion_diagrama, name='importar_diagrama'),
    path('edicion_diagrama/', edicion_diagrama, name='edicion_diagrama'),
    path('edicion_diagrama/<id>', edicion_diagrama, name='edicion_diagrama'),
    path('documentos/', documentos, name='documentos'),
    path('edicion_diagrama_importado/<id>/', conversion, name='conversion'),
]


$(function principal() {

    /* Se llama al constructor graph */
    var graph = new joint.dia.Graph;

    /* Se añade el papel o zona de diseño y sus propiedades */
    var paper = new joint.dia.Paper({
        el: document.getElementById('zona_edicion'),
        model: graph,
        width: 1350,
        height: 800,
        gridSize: 20,
        drawGrid: true,
        background: {
            color: 'rgba(255, 99, 71, 0)'
        },
        restrictTranslate: true, //Se restingue el papel al contorno de la ventana
        interactive: { //Se desactiva la creación interactiva de vertices en el enlace
            vertexAdd: false
        }
    });

    var uml = joint.shapes.uml;

    /* Se importa si es viene algun dato desde la url */
    var importar = document.getElementById('data');
    if (importar.innerText != ""){

        archivo = JSON.parse(importar.innerText)
        
        // atributos
        archivo.cells.forEach(element => {
            if (element.type == 'standard.Ellipse'){
                console.log(element.attrs.label.text) // obtengo el nombre del atributo
                // obtener el source, a quien esta conectado.

            }
        });

        // entidades
        archivo.cells.forEach(element => {
            if (element.type == 'standard.Rectangle'){
                creacion_JSON(element.attrs.label.text, element.id, element.position.x, element.position.y)
            } 
        });

    }

    function creacion_JSON(entidad, id, x, y) {

        var classes = {

            entidad : new uml.Class({
                position: { x:630  , y: 190 },
                id: id,
                size: { width: 160, height: 100 },
                name: entidad,
                position: {"x": x, "y": y},
                attributes: [],
                methods: [],
                attrs: {
                    '.uml-class-name-rect': {
                        fill: '#ff8450',
                        stroke: '#fff',
                        'stroke-width': 0.5
                    },
                    '.uml-class-attrs-rect': {
                        fill: '#fe976a',
                        stroke: '#fff',
                        'stroke-width': 0.5
                    },
                    '.uml-class-methods-rect': {
                        fill: '#fe976a',
                        stroke: '#fff',
                        'stroke-width': 0.5
                    },
                    '.uml-class-attrs-text': {
                        'ref-y': 0.5,
                        'y-alignment': 'middle'
                    }
                }

            }),
        };

        Object.keys(classes).forEach(function(key) {
            graph.addCell(classes[key]);
        });



    }

}());
$(function principal() {

    /*var dict = new Object();

    var dict = {
        
    }*/
    
    /* Se llama al constructor graph */
    var graph = new joint.dia.Graph;

    /* Se añade el papel o zona de diseño y sus propiedades */
    var paper = new joint.dia.Paper({
        el: document.getElementById('zona_edicion'),
        model: graph,
        width: 1350,
        height: 800,
        gridSize: 20,
        drawGrid: true,
        background: {
            color: 'rgba(255, 99, 71, 0)'
        },
        restrictTranslate: true, //Se restingue el papel al contorno de la ventana
        interactive: { //Se desactiva la creación interactiva de vertices en el enlace
            vertexAdd: false 
        }
    });

    /* Creación de un nuevo elemento para la entidad asociativa */
    joint.dia.Element.define('erd.Entity_AS', {
        size: { width: 110, height: 50 }, //Se define un tamaño
        attrs: {
            '.inner': { //Internamente se observa un poligono
                fill: 'white', //Relleno color blanco
                stroke: 'black', //COntorno negro
                points: '0,50 50,0 98,50 50,98', //Puntos de referencia del poligono
                filter: { name: 'dropShadow',  args: { dx: 0.5, dy: 2, blur: 2, color: '#333333' }},
            },
            '.outer': { //Externamente se observara un rectangulo
                fill: 'white',  //Color interno blanco
                stroke: 'black', //Color contorno negro
                points: '100,0 100,100 0,100 0,0' //Puntos de referencia del rectangulo
            },
            text: { //Se define que el texto quede centrado en el elemento
                refX: "50%",
                refY: "50%",
                textAnchor: "middle",
                textVerticalAnchor: "middle",
            },
        }
    }, {
        markup: '<g class="rotatable"><g class="scalable"><polygon class="outer"/><polygon class="inner"/></g><text/></g>',
    });

    /*Botones -> Al hacer click en el boton activar la función */
    document.getElementById("entidad").onclick = function() {agregar_elemento(graph, paper, 'Rectangle')};
    document.getElementById("relacion").onclick = function() {agregar_elemento(graph, paper, 'Polygon')};
    document.getElementById("atributo").onclick = function() {agregar_elemento(graph, paper, 'Ellipse')};
    document.getElementById("entidad_asociativa").onclick = function() {agregar_elemento(graph, paper, 'Entidad_Asociativa')};
    document.getElementById("guardar").onclick = function() {guardar(graph)}; // Click en boton guardar 
    document.getElementById("eliminar_todo").onclick = function() {eliminar_todo(graph)}; // Click en boton eliminar todo
    document.getElementById("exportar_img").onclick = function() {export_img(paper)}; // Click en boton eliminar todo
    
    /* Agregar un elemento */
    function agregar_elemento(graph, paper, tipo_element) {
        
        var element;

        if (tipo_element == 'Ellipse'){ //Si es del tipo ellipse
            element = new joint.shapes.standard.Ellipse(); //Crear la variable elemento con forma de atributo
            window_add_name_element('Atributo', element); //FUnción que levanta ventana emergente

        }else if (tipo_element == 'Rectangle'){ //Si es del tipo rectangle            
            element = new joint.shapes.standard.Rectangle();
            window_add_name_element('Entidad', element); //Función que levanta ventana emergente

        }else if (tipo_element == 'Polygon'){  //Si es del tipo polygon
            element = new joint.shapes.standard.Polygon();
            window_add_name_element('Relación', element); //Función que levanta ventana emergente

        }else if (tipo_element == 'Entidad_Asociativa'){
            element = new joint.shapes.erd.Entity_AS(); //Si es del tipo entidad asociativa
            window_add_name_element('Entidad Asociativa', element); //Función que levanta ventana emergente
        }


        element.attr({          //Configuración de atributos generales
            body: {
                fill: '#ffffff',
                refPoints: '0,10 10,0 20,10 10,20'
            },
            label: {
                fill: 'black',
                fontSize: 16
            },
            text: {
                text: '',
                'text-decoration': 'none'
            }
        });

        element.position(10, 10);  //Posicion del elemento creado
        element.resize(110, 50); //Tamaño del elemento

        element.addTo(graph);  //Se añade el elemento al grafico
        
        var elementos_graph = graph.getElements();   //Obtiene los elementos dentro del modelo

        for (var i = 0, ii = elementos_graph.length; i < ii; i++) { //Recorre los elementos
            var elemento_actual = elementos_graph[i];
            herramientas_y_complementos(graph, paper, elemento_actual); //Se llama la funcion que añade herramientas
        
        }

    }

    
    /* Editar los atributos de los elementos */
    function editar(element) {
        
        const nombre_element = element.attr('label/text');
        const nombre_element_EA = element.prop('attrs/text/text');

        const type = element.prop('type');

        if (type == 'standard.Ellipse'){
            window_edit_atributo('Atributo', element, nombre_element);
        }else if (type == 'standard.Rectangle'){
            window_edit_element('Entidad', element, nombre_element);
        }else if (type == 'standard.Polygon'){
            window_edit_element('Relación', element, nombre_element);
        }else if (type == 'erd.Entity_AS'){
            window_edit_element('Entidad Asociativa', element, nombre_element_EA);
        }
    
    }

    /* Ventana emergente al editar un elemento tipo entidad o relación */
    function window_edit_atributo(tipo_element, element, nombre_element){
        Swal.fire({ //Se utiliza un sweetAlert para nombrar el atributo
            title: tipo_element+' "'+nombre_element+' "',
            allowOutsideClick: false,
            showCancelButton: true,
            showDenyButton: true,
            denyButtonText: "Propiedades", //Boton agregar atributo
            denyButtonColor: '#9e0af0',
            confirmButtonText: "Editar",
            confirmButtonColor: '#499a00',
            cancelButtonText: "Cerrar",
            cancelButtonColor: '#ff4f3c',
        }).then(resultado => {
            if (resultado.isConfirmed) {
                window_add_name_element('Atributo', element); //FUnción que levanta ventana emergente
            }else if(resultado.isDenied) {
                Swal.fire({
                    title: "Selecciona la propiedad",
                    input: "select",
                    inputOptions: {
                      "0": "Absoluto",
                      "1": "Clave primaria",
                      "2": "Opcional",
                    },
                    showCancelButton: true,
                    inputValidator: function (value) {
                      return new Promise(function (resolve, reject) {
                        if (value !== '') {
                          resolve();
                        } else {
                          resolve('Debes seleccionar una opción!');
                        }
                      });
                    }
                }).then(resultado_select =>  {
                    if (resultado_select.value == "0") { //Atributo absoluto
                        if (nombre_element.includes('(O)') == true){ //Si incluye el caracter (O), se ingresa para eliminarlo
                            var eliminar_opcional = nombre_element.slice(0, nombre_element.length - 3);
                            element.attr('label/text', eliminar_opcional); //Se añade el nuevo texto
                        }
                        element.attr('text/text-decoration', 'none');
                    }else if (resultado_select.value == "1"){ //Atributo clave primaria
                        if (nombre_element.includes('(O)') == true){ //Si incluye el caracter (O), se ingresa para eliminarlo
                            var eliminar_opcional = nombre_element.slice(0, nombre_element.length - 3);
                            element.attr('label/text', eliminar_opcional); //Se añade el nuevo texto
                        }
                        element.attr('text/text-decoration', 'underline');
                    }else if (resultado_select.value == "2"){ //Atributo opcional
                        element.attr('text/text-decoration', 'none');
                        element.attr('label/text', nombre_element+"(O)"); //Se añade el nuevo texto
                    }
                });
            }
        })
    }

    /* Agregar atributo a un elemento*/
    function agregar_atributo_element(graph, paper, elementX) {
        
        var element = new joint.shapes.standard.Ellipse(); //Crear la variable elemento con forma de atributo

        element.position(10, 10);  //Posicion y atributos del elemento creado
        element.resize(100, 40); //Tamaño de los elementos

        window_add_name_element('Atributo', element); //FUnción que levanta ventana emergente

        element.attr({          //Configuración de atributos generales
            body: {
                fill: '#ffffff',
                refPoints: '0,10 10,0 20,10 10,20'
            },
            label: {
                fill: 'black',
                fontSize: 16
            },
            text: {
                text: ''
            }
        });

        element.addTo(graph);  //Se añade el elemento al grafico

        enlace_element_atributo(graph, element, elementX);

        herramientas_y_complementos(graph, paper, element); //Se llama la funcion que añade herramientas
        
    }


    /*Ventana emergente al añadir elementos*/
    function window_add_name_element(tipo_element, element){      
        var nombre_elemento;
        if (tipo_element == 'Entidad Asociativa'){
            nombre_elemento = element.prop('attrs/text/text');
        }else{
            nombre_elemento = element.attr('label/text');
        }

        Swal.fire({ //Se utiliza un sweetAlert para nombrar el elemento
            title: "Nombrar " + tipo_element,
            input: "text",
            inputPlaceholder:  nombre_elemento,
            allowOutsideClick: false,
            showCancelButton: true,
            confirmButtonText: "Agregar",
            confirmButtonColor: '#499a00',
            cancelButtonText: "Cerrar",
            cancelButtonColor: '#ff4f3c',
        }).then(resultado => {
            let nombre = resultado.value;
            var text = joint.util.breakText(nombre, { width: 90, height:40})  //Controlar el largo del texto con breakText
            if (tipo_element == 'Atributo') {
                var text_minus = text.toLowerCase();
                element.attr('label/text', text_minus); //Se añade el nuevo atributo
            }else if (tipo_element == 'Entidad Asociativa') {
                var text_mayus = text.toUpperCase();
                element.prop('attrs/text/text', text_mayus); //Se añade el nuevo texto
            }else{
                var text_mayus = text.toUpperCase();
                element.attr('label/text', text_mayus); //Se añade el nuevo texto
            }
        });

    }

    /* Ventana emergente al editar un elemento tipo entidad o relación */
    function window_edit_element(tipo_element, element, nombre_element){
        Swal.fire({ //Se utiliza un sweetAlert
            title: tipo_element+' "'+ nombre_element+'"',
            allowOutsideClick: false,
            showCancelButton: true,
            showDenyButton: true,
            denyButtonText: "Agregar Atributo", //Boton agregar atributo
            denyButtonColor: '#9e0af0',
            confirmButtonText: "Editar",   //Boton agregar
            confirmButtonColor: '#499a00',  
            cancelButtonText: "Cerrar", //Boton cancelar
            cancelButtonColor: '#ff4f3c',
        }).then(resultado => {
            if (resultado.isConfirmed) {
                if (tipo_element == 'Entidad'){
                    window_add_name_element('Entidad', element); //Función que levanta ventana emergente
                }else if (tipo_element == 'Entidad'){
                    window_add_name_element('Relación', element); //Función que levanta ventana emergente
                }else {
                    window_add_name_element('Entidad Asociativa', element);
                }
                
            }else if(resultado.isDenied) {
                agregar_atributo_element(graph, paper, element);
            }
        });
    }


    /* Enlaces entre elementos */
    function enlace_element_atributo(graph, element, elementX){

        var enlace = new joint.shapes.erd.Line(); // Crea la conexion entre dos elementos con el complemento ERD
        enlace.source(element); // Enlace hacia el elemento fuente
        enlace.target(elementX); // Enlace hacia el elemento objetivo
        enlace.addTo(graph); // Se añade el enlace

    }

    /* Función que resetea el elemento al hacer click */
    function resetea_elemento(paper) { 
        paper.drawBackground({  //Cambia el color
            color: 'white'
        })
    
        var elements = paper.model.getElements();   //Obtiene los elementos dentro del modelo
        for (var i = 0, ii = elements.length; i < ii; i++) { //Recorre los elementos
            var currentElement = elements[i];
            const type = currentElement.prop('type');
            if (type == 'erd.Entity_AS'){
                currentElement.prop('attrs/.outer/stroke', 'black');
            }else{
                currentElement.attr('body/stroke', 'black');
            }
        }

    }

    /* Se agregan botones y complementos */
    function herramientas_y_complementos(graph, paper, element){
        
        var herramienta_contorno = new joint.elementTools.Boundary({ //Boton delimitador del elemento
            useModelGeometry: true, //Atributos, configuraciones
        }); 
        
        var eliminar_interactivo = new joint.elementTools.Remove({ //Boton eliminar interactivo
            focusOpacity: 0.5, //Atributos, configuraciones
            rotate: true,
            x: '0%',
            y: '0%',
            offset: { x: 50, y: 0 }
            }
        );

        var vista_herramientas = new joint.dia.ToolsView({  //Variable de herramientas
            tools: [herramienta_contorno, eliminar_interactivo]
        });

        var vista_elemento = element.findView(paper).addTools(vista_herramientas).hideTools(); 

        paper.on('element:mouseenter', function(vista_elemento) { //Cuando el mouse se acerca se muestra
            vista_elemento.showTools();
        });

        paper.on('element:mouseleave', function(vista_elemento) { //Cuando el mouse se aleja, se oculta
            vista_elemento.hideTools();
        });

        paper.on('cell:pointerclick', function(cellView){ //Selecciona un elemento al hacer un click    
            resetea_elemento(this); //Se resetea el elemento
            var elemento_actual = cellView.model;
           
            const type = elemento_actual.prop('type');
            if (type == 'erd.Entity_AS'){
                elemento_actual.prop('attrs/.outer/stroke', 'orange');
            }else{
                elemento_actual.attr('body/stroke', 'orange')       
            }
            document.getElementById("eliminar_elemento").onclick = function() {eliminar_elemento(elemento_actual)}; // Click en boton eliminar elemento
        });

        paper.on('cell:pointerdblclick', function(cellView) { //Cuando se hacen dos click permite la edición de atributo
            var elemento_actual = cellView.model;
            var nombre_elemento;
            const type = element.prop('type');

            if (type == 'erd.Entity_AS'){
                nombre_elemento = elemento_actual.prop('attrs/text/text');
            }else{
                nombre_elemento = elemento_actual.attr('label/text');
            }
            editar(elemento_actual);          
        });

        paper.on({'element:pointerdown': function(vista_elemento, evt) { //Enlaces entre elementos
            evt.data = vista_elemento.model.position();
            },

            'element:pointerup': function(vista_elemento, evt, x, y) { //Realizar conexion al dejar caer un elemento sobre otro

                var coordenadas = new g.Point(x, y); //Coordendas del elemento
                var elemento_fuente = vista_elemento.model;
                var elemento_objetivo = this.model.findModelsFromPoint(coordenadas).find(function(el) {
                    return (el.id !== elemento_fuente.id); 
                });

                //Si dos elementos ya estan conectados no se conectan de nuevo
                if (elemento_objetivo && graph.getNeighbors(elemento_objetivo).indexOf(elemento_fuente) === -1) {

                    elemento_fuente.position(evt.data.x, evt.data.y);  // Mover el elemento a la posicion antes de arrastrar

                    var createLabel = function(txt) {
                        return {
                            labels: [{
                                position: {
                                    distance: 0.2, //Distancia del x label
                                    offset: 10 //Distancia y del label
                                },
                                attrs: {
                                    text: {text: txt, fill: 'black' },
                                    rect: { fill: 'none' }
                                }
                            }]
                        };
                    };

                    var link = new joint.shapes.erd.Line(); // Crea la conexion entre dos elementos con el complemento ERD
                    link.source(elemento_fuente); // Enlace hacia el elemento fuente
                    link.target(elemento_objetivo); // Enlace hacia el elemento objetivo
                    link.addTo(graph); // Se añade el enlace
                    
                    paper.on('link:pointerdblclick', function(){ //Si se hace doble click en el enlace
                        Swal.fire({                             //Se levanta la ventana de cardinalidad
                            title: "Editar Cardinalidad",
                            input: "select",
                            inputOptions:{
                                'unoAuno': '1..1 (Uno a Uno)',
                                'ceroAmuchos': '0..N (Cero a Muchos)',
                                'uno': '1 (Uno)',
                                'muchos': 'N (Muchos)'
                            },
                            inputPlaceholder: 'Elegir',
                            showCancelButton: true,
                            confirmButtonText: "Editar",
                            cancelButtonText: "Cancelar",
                        }).then(resultado => {
                            if (resultado.value == 'unoAuno') {
                                link.source(elemento_fuente).set(createLabel('1..1'));
                            }else if (resultado.value == 'ceroAmuchos') {
                                link.source(elemento_fuente).set(createLabel('0..N'));
                            }else if (resultado.value == 'uno') {
                                link.source(elemento_fuente).set(createLabel('1'));
                            }else if (resultado.value == 'muchos') {
                                link.source(elemento_fuente).set(createLabel('N'));
                            }
                        });       
                    });

                }
            }
        });        
    }  

    function cardinalidad_enlace(){

    }

    /* Guardar un diagrama en formato JSON */
    function guardar(graph){
        graph.set('graphCustomProperty', true);
        graph.set('graphExportTime', Date.now());
        
        var diagrama = JSON.stringify(graph.toJSON()); //Se añaden los datos seteados a la variable archivo

        var nombre_archivo = "diagrama.json"; //Nombre predeterminado para el archivo
        var element = document.createElement('a');
        element.setAttribute('href','data:text/json;charset=utf-8, ' + encodeURIComponent(diagrama));
        element.setAttribute('download', nombre_archivo);
        
        document.body.appendChild(element);
        element.click();
        document.body.removeChild(element);
    }

   
    /* Funcion que importa un diagrama y lo procesa*/
    function subirDiagrama(id, callback) {
        document.getElementById(id).onchange = function(evt) {
            try {
                let files = evt.target.files;
                if (!files.length) {
                    alert('No se ha detectado archivo');
                    return;
                }
                let file = files[0];
                let reader = new FileReader();
                const self = this;
                reader.onload = (event) => {
                    callback(event.target.result);
                };
                reader.readAsText(file);
            } catch (err) {
                console.error(err);
            }
        }
    }
    
    /* Subir diagrama, con el retorno de la funcion */
    subirDiagrama('importJson', function (json) {
        graph.fromJSON(JSON.parse(json));
        
    });

    
    /* Función eliminar todo*/
    function eliminar_todo(graph){

        Swal.fire({ //Se utiliza un sweetAlert para nombrar la entidad
            title: 'Estas segur@?',
            text: "No podrás revertir esto!!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: "Salir",
            confirmButtonText: 'Sí, elimina todo!'
        }).then(resultado => {
            if (resultado.isConfirmed) {
                graph.clear();
            }
        });

    }

    /* Función que elimina un elemento */
    function eliminar_elemento(elemento_actual){
        const type = elemento_actual.prop('type'); //Sa obtiene el tipo de elemento

        if (type == 'standard.Rectangle'){ //Si es del tipo entidad
            var elements = paper.model.getElements();   //Obtiene los elementos dentro del modelo
            for (var i = 0, ii = elements.length; i < ii; i++) { //Recorre los elementos
                if (elements[i] && graph.getNeighbors(elements[i]).indexOf(elemento_actual) === 0){ //Si esos elementos estan conectados al elemento actual
                    var elemento_a = elements[i];
                    const type_elem = elemento_a.prop('type'); //Sa obtiene el tipo de elemento
                    if (type_elem == 'standard.Ellipse'){ //Si es del tipo entidad
                        elemento_a.remove();
                    }
                }
            }
            elemento_actual.remove(); //Finalmente se remueve el elemento actual
        }else if(type == 'standard.Ellipse' || type == 'standard.Polygon' || type == 'erd.Entity_AS'){ //Si es tipo atributo o relación
            elemento_actual.remove();
        }
    }


    /*Función que exporta diagrama a imagen */
    function export_img(paper){
        
        paper.fitToContent({    //Obtiene un tamaño de papel adecuado a los elementos
            padding: 20,    //Se define un margen
            allowNewOrigin: 'any', //Se adapta al contenido, no al origen (0,0)
        });

        var dimensiones_paper = paper.getComputedSize(); //Se obtienen las dimensiones del papel con elementos
        var dimensiones = Object.entries(dimensiones_paper);   //Se obtiene la matriz de propiedades
        const width_paper = Object.values(dimensiones['0']).slice(1, 2); //Obtiene la dimension width
        const height_paper = Object.values(dimensiones['1']).slice(1, 2); //Obtiene la dimension height

        //Se remueven las herramientas para la imagen
        $(".marker-arrowhead").css("display", "none");
        $(".tool-remove").css("display", "none");
        $(".tool-options").css("display", "none");
        $(".marker-vertices").css("display", "none");

        let documento_svg = paper.svg; //Se obtiene el papel en SVG
        let serializacion = new XMLSerializer(); //Se serializan los elementos
        let cadena_svg = serializacion.serializeToString(documento_svg); //Se convierten en una cadena

        let domUrl = window.URL || window.webkitURL || window;
        let canvas = document.getElementById("idcanvas"); //Se obtiene el lienzo al que se pasara la imagen
        canvas.width  = width_paper;
        canvas.height = height_paper;
        let ctx = canvas.getContext("2d"); //Retorna un dibujo en el lienzo
        
        let svg = new Blob([cadena_svg], { //Se convierte la cadena en un Blob para ser codificados
          type: "image/svg+xml;charset=utf-8" //
        });

        let url = domUrl.createObjectURL(svg); //Crea la url del blob
        

        let img = new Image; //Crea una nueva imagen
        img.onload = function(){ //Cuando se carga la imagen, se puede obtener como base64 url
            ctx.drawImage(this, 0, 0);
            domUrl.revokeObjectURL(url); //Se remueve el objeto original

            var imagen = canvas.toDataURL("image/png"); //Se obtiene la imagen
            var enlace_temporal = document.createElement( 'a' );  //Se crea un enlace temporal
            enlace_temporal.download = 'diagrama.png';  //Nombre que tendrá la imagen
            enlace_temporal.href = imagen;  //Se crea la imagen a partir del lienzo y se agrega una url temporal

            document.body.appendChild(enlace_temporal);  //Se asigna el enlace a la descarga
            enlace_temporal.click();  
            document.body.removeChild(enlace_temporal);
        };     
        img.src = url;

        paper.fitToContent({ //Vuelve el tamaño inicial definido en el papel
            minWidth: 1350, 
            minHeight: 800, 
        });
      
    }
   

}());